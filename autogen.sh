#!/bin/sh -e

if [ -f .git/hooks/pre-commit.sample -a ! -f .git/hooks/pre-commit ] ; then
        cp -p .git/hooks/pre-commit.sample .git/hooks/pre-commit && \
        chmod +x .git/hooks/pre-commit && \
        echo "Activated pre-commit hook."
fi

if which gtkdocize >/dev/null 2>/dev/null; then
    gtkdocize --copy
else
    echo "You don't have gtk-doc installed, documentation generation disabled."
    # THE FOLLOWING IS UGLY
    rm gtk-doc.make
    echo 'EXTRA_DIST =' > gtk-doc.make
fi

autoreconf --install --symlink

libdir() {
        echo $(cd $1/$(gcc -print-multi-os-directory); pwd)
}

args="$args \
--prefix=/usr \
--sysconfdir=/etc \
--libdir=$(libdir /usr/lib)"

if [ -L /bin ]; then
args="$args \
--libexecdir=/usr/lib \
"
else
args="$args \
--with-rootprefix= \
--with-rootlibdir=$(libdir /lib) \
--bindir=/sbin \
--libexecdir=/lib \
"
fi

echo
echo "----------------------------------------------------------------"
echo "Initialized build system. For a common configuration please run:"
echo "----------------------------------------------------------------"
echo
echo "./configure CFLAGS='-g -O1' $args"
echo
